#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all reproducible=+fixfilepath

include /usr/share/dpkg/architecture.mk

%:
	dh $@ --buildsystem=cmake

override_dh_auto_configure:
	dh_auto_configure --buildsystem=cmake --                        \
		-DINCLUDE_INSTALL_DIR=include                           \
		-DLIB_INSTALL_DIR=lib/$(DEB_HOST_MULTIARCH)             \
		-DBIN_INSTALL_DIR=bin                                   \
		-DMAN_INSTALL_DIR=share/man/man1                        \
		-DDOC_INSTALL_DIR=share/doc/libqhull-dev/qhull

override_dh_auto_build:
	dh_auto_build
	@echo Building man pages
	cd debian &&							\
	  for info in "qconvex:compute the convex hull"			\
	              "qdelaunay:compute the Delaunay triangulation"	\
	              "qhalf:halfspace intersection about a point"	\
	              "qvoronoi:compute the Voronoi diagram" ; do	\
	      comm=$$(echo  $$info | cut -d: -f1) &&			\
	      purp=$$(echo  $$info | cut -d: -f2) &&			\
	      echo -n Building man page for $$comm... &&		\
	      sed "s/#command#/$$comm/;s/#purpose#/$$purp/"		\
	          manpage.in > $$comm.xml &&				\
	      docbook2x-man $$comm.xml &&				\
	      echo " done" ;						\
	  done

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	env PATH=$(CURDIR)/obj-$(DEB_HOST_GNU_TYPE):$${PATH} eg/q_test
endif

override_dh_auto_install:
	dh_auto_install --buildsystem=cmake
	# No need for extra copy of license
	rm --verbose debian/tmp/usr/share/doc/libqhull-dev/qhull/COPYING.txt
	# No need for doc/ copies of man pages in ../../man/man1/ or for other formats.
	@for f in qhull rbox; do \
	  for ext in man txt htm; do \
	    rm --verbose debian/tmp/usr/share/doc/libqhull-dev/qhull/$$f.$$ext || true; \
	  done \
	done
	# Move documentation out of include dir
	mkdir -p debian/tmp/usr/share/doc/libqhull-dev/include
	mv --verbose debian/tmp/usr/include/libqhull/*.htm debian/tmp/usr/share/doc/libqhull-dev/include
	rm --verbose debian/tmp/usr/include/libqhull_r/*.htm
	# Remove RPATH
	find debian/tmp/usr/lib -type f -name '*.so*' \
	 -exec chrpath --list '{}' ';' \
	 -exec chrpath --delete '{}' ';'

override_dh_installchangelogs:
	dh_installchangelogs src/Changes.txt

override_dh_link:
	mkdir -p debian/libqhull-doc/usr/share/doc/libqhull-dev/include
	@for f in $$(cd debian/tmp && echo usr/include/libqhull/*.h); do \
	  ln --verbose -s /$$f debian/libqhull-doc/usr/share/doc/libqhull-dev/include/; \
	done
	dh_link

override_dh_compress:
	dh_compress -X.c -X.h -X.txt
